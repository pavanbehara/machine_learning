import numpy as np

rating_matrix = np.zeros((10, 10))  # index[0] => 0.5 ... index[9] => 5

for i in range(rating_matrix.shape[0]):
    for j in range(rating_matrix.shape[1]):
        rating_difference = abs(i-j)/2.0
        c = -0.03704*np.square(rating_difference) - 0.02222 * rating_difference + 0.9
        rating_matrix[i][j] = c

class User:

    def __init__(self):
        self.user_id = "null"
        self.training_review_dict = {}
        self.testing_review_dict = {}
        self.user_friends_list = []
        self.alpha = 1
        self.k_value = 1
        self.mean_squared_error = 0
        self.user_prior = np.full(10, 0.1, dtype=np.float) # probability of rating - user's rating history
        self.user_friends_conditional_prob = rating_matrix  # probability of j rating given by friend when user gives i


