import json
import pickle
import sys
import numpy as np
from User import User
from Business import Business
degree = 1
sys.setrecursionlimit(30000000)

def main():
    
    user_file = open("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_user.json", "r")
    review_file = open("yelp_dataset_challenge_academic_dataset/yelp_academic_dataset_review.json", "r")

    target_user_dict = {}
    user_id_reviews_dict = {}
    custom_user_list = []

    # Creating a custom user list using user and review json files 
    # from a total of 686556 users and 2685066 reviews
    # Users with friends >= 5 are considered since we are doing a social network analysis
    # Also, user_friends_list is populated with up to 20 friends for easy computation
    # This populate() step takes around 2-3 minutes on a single core of Intel(R) Xeon(R) CPU L5630 @ 2.13GHz
 
    populate(user_file, review_file, target_user_dict)

###############################################################################################################

    algo_usercount = 0 #just to keep track of progress during training stage
    print("about to enter the training for loop")
    sys.stdout.flush()

    # For every user_Id the user_prior depends on alpha and the user_friend_cond_prob prior on K value
    # In the training phase alpha and K values are optimized on a grid of values
    # For alpha seven values are chosen [  1.00000000e-03,   6.81292069e-03,   4.64158883e-02, 3.16227766e-01,   2.15443469e+00,   1.46779927e+01,
    #    1.00000000e+02], alpha=1 would be same as not applying prior so it is avoided
    # We tried out many values for K but we couldn't see any difference in the error due to K
    # we finally took [  1.00000000e-03,   4.64158883e-02,   2.15443469e+00,  1.00000000e+02] for K 
    
    # The training part of code takes around 30-40 minutes for completion on the same hardware as above
    # 70% of the reviews were considered as training_set
 
    for userId_Key in target_user_dict.keys():

        error_result_matrix = np.zeros((4, 4))   # for finding the minimum error on the grid
        alpha_index = 0

        temp_star = []                           # stars of the user stored in a temporary list
        user_business_dict = {}                  # contains training set of user business ids, ratings
        user_object = target_user_dict.get(userId_Key)     

        for user_business_id in user_object.training_review_dict:
            business = user_object.training_review_dict[user_business_id]
            if not(business is None):
                temp_star.append(business.stars)
                user_business_dict[business.business_id] = business

        rating_count_array = np.zeros(10)       # index[0] => 0.5 ... index[9] => 5
 
        # Starting the optimization loop

        for alpha in np.logspace(1, 4, num=4):
            k_index = 0
            for k in np.arange(1, 5, 1):
                for val in np.arange(0.5,5.5,0.5):
                    ind_val = int(val*2.0 - 1.0)
                    rating_count_array[ind_val] = (temp_star.count(val) + alpha - 1)
                    
                sum_user_vals = sum(rating_count_array)
                user_prior_matrix = rating_count_array/sum_user_vals  #prior probability of the user giving a particular rating (from 0.5 - 5.0)

                # The user_friend_conditional_probability is also initialized below

                rating_ji = np.multiply(k, user_object.user_friends_conditional_prob)
                multiply_rating_ji = np.ones(10)
                mean_squared_error = 0
                
                # The for loop below will iterate over all the common businesses and the occurences would be added to the conditional probability prior

                for user_business in user_business_dict.keys():
                    for friend in user_object.user_friends_list:
                        if not(friend is None) and user_business in friend.training_review_dict.keys():
                            j_star = friend.training_review_dict[user_business].stars  # star for a business from friend review object
                            i_star = user_business_dict[user_business].stars
                            ind_j = int(j_star*2.0 - 1.0)   # friend column
                            ind_i = int(i_star*2.0 - 1.0)   # user row
                            rating_ji[ind_i][ind_j] += 1
                    rating_ji = rating_ji/np.sum(rating_ji)
                
                # The for loop below would predict the rating for a business in the user_training_list
                # P(r_A | r_J) * P(r_B | r_J) * P(r_C | r_J) * P(r_J) / SUM (...)  
                # P(r_A | r_J) is a 10x1 array for conditional probabilies for a specific r_A (say 2.0)
                # given r_J would be {0.5, 1.0, ..., 5.0} 
                # Element-wise multiplication for P(r_A = 2.0 | r_J={all}) * P(r_B = 3.0 | r_J = {all}) and so on
                # multiply_rating_ji would capture this by multiplying columns for each of A, B, C
                # After all the probabilities are multiplied they are normalized too  
                
                # Predicted rating is obtained by taking the expected value and also rounding off to nearest *.5 star         
                 
                for user_business in user_business_dict.keys():
                    for friend in user_object.user_friends_list:
                        if not(friend is None) and user_business in friend.training_review_dict.keys():
                            j_star = friend.training_review_dict[user_business].stars  # star for a business from friend review object
                            i_star = user_business_dict[user_business].stars
                            ind_j = int(j_star*2.0 - 1.0)   # friend column
                            ind_i = int(i_star*2.0 - 1.0)   # user row
                            multiply_rating_ji = np.multiply(multiply_rating_ji, np.transpose(rating_ji[:,ind_j]))

                    user_bayes_prob = np.multiply(multiply_rating_ji, user_prior_matrix)
                    sum_user_bayes_prob = np.sum(user_bayes_prob)
                    if (sum_user_bayes_prob == 0):
                        user_bayes_prob = user_object.user_prior 
                    else:
                        user_bayes_prob = user_bayes_prob/sum_user_bayes_prob
                    predicted_rating = 0
                    for i in range(0, 10):
                        predicted_rating += (i+1)/2 * user_bayes_prob[i]
                    predicted_rating = round(predicted_rating*2)/2
                    # print("Predicted rating is ", predicted_rating)
                    mean_squared_error += np.square((predicted_rating - user_business_dict[user_business].stars))

                # Mean of the squared error for all the businesses is taken
                # If there are no business ratings then the user prediction would be based on the prior only

                valid_business_count = len(user_business_dict.keys())
                if valid_business_count == 0:
                    user_object.mean_squared_error = mean_squared_error
                else:
                    user_object.mean_squared_error = (mean_squared_error / valid_business_count)
                   
                error_result_matrix[alpha_index][k_index] = user_object.mean_squared_error

                k_index += 1
            alpha_index += 1


        # checking for the (alpha, K) pair that would give the minimum error
        # updating the user_object with that pair so that these values can be 
        # used for testing purposes

        min_index_pair = np.unravel_index(error_result_matrix.argmin(), error_result_matrix.shape)
        user_object.alpha = np.logspace(1, 4, num=4)[min_index_pair[0]]
        user_object.k_value = np.arange(1, 5, 1)[min_index_pair[1]]

        
        rating_count_array = np.zeros(10)

        for val in np.arange(0.5,5.5,0.5):
            ind_val = int(val*2.0 - 1.0)
            rating_count_array[ind_val] = (temp_star.count(val) + user_object.alpha - 1)

        sum_user_vals = sum(rating_count_array)
        user_object.user_prior = rating_count_array/sum_user_vals       #Pr_i = prob that user gives rating i

        rating_ji = np.multiply(user_object.k_value, user_object.user_friends_conditional_prob)              

        for user_business in user_business_dict.keys():
            for friend in user_object.user_friends_list:
                if not(friend is None) and user_business in friend.training_review_dict.keys():
                    j_star = friend.training_review_dict[user_business].stars  # star for a business from friend review object
                    i_star = user_business_dict[user_business].stars
                    ind_j = int(j_star*2.0 - 1.0)   # friend column
                    ind_i = int(i_star*2.0 - 1.0)   # user row
                    rating_ji[ind_i][ind_j] += 1

            rating_ji = rating_ji/np.sum(rating_ji)
            user_object.user_friends_conditional_prob = rating_ji        #Pr_ji = prob that friend gives j when user gives i

        algo_usercount +=1
        if(algo_usercount%1000)==0:
            print(algo_usercount)
            sys.stdout.flush()

    print("completed training")

###############################################################################################################
    testing_total_error = 0.0 
    count_ratings = 0.0

    # Testing_dataset 
    # For every business in the user's testing set the rating is predicted using 
    # the user_prior and user_friends_conditional_prob stored in every user object
    # for all the direct friends in the user_friends_list
    # In case the business is not rated by the friend then the second degree friend's rating is used
    # to predict the direct friend's rating and then this rating is used to derive the final User Baye's probability 
    
    for userId_Key in target_user_dict.keys():
        user_business_dict = {}
        user_object = target_user_dict.get(userId_Key)
        
        for user_business_id in user_object.testing_review_dict:
            business = user_object.testing_review_dict[user_business_id]
            if not(business is None):
                user_business_dict[business.business_id] = business
        valid_business_count = len(user_business_dict.keys())
        if (valid_business_count != 0):
            mean_squared_error = 0
            for user_business in user_business_dict.keys():
                multiply_rating_ji = np.ones(10)
                for friend in user_object.user_friends_list:
                    if not(friend is None) and not (user_business is None):
                        if user_business in friend.testing_review_dict.keys():
                            j_star = friend.testing_review_dict[user_business].stars  # star for a business from friend review object
                            ind_j = int(j_star*2.0 - 1.0)   # friend column
                            multiply_rating_ji = np.multiply(multiply_rating_ji, np.transpose(
                                                                        user_object.user_friends_conditional_prob[:, ind_j]))
                        elif (degree == 2 ): #this will go into second degree friends only when degree is given as 2
                            second_degree_multiply_rating_ji = np.ones(10)
                            for second_degree_friend in friend.user_friends_list:
                                if not(second_degree_friend is None) \
                                        and user_business in second_degree_friend.testing_review_dict.keys():
                                    j_star = second_degree_friend.testing_review_dict[user_business].stars  # star for a business from friend review object
                                    ind_j = int(j_star*2.0 - 1.0)   # friend column
                                    second_degree_multiply_rating_ji = np.multiply(second_degree_multiply_rating_ji,
                                                                    np.transpose(friend.user_friends_conditional_prob[:, ind_j]))
                            friend_bayes_prob = np.multiply(second_degree_multiply_rating_ji, friend.user_prior)
                            sum_friend_bayes_prob = np.sum(friend_bayes_prob)
                            if(sum_friend_bayes_prob == 0):
                                friend_bayes_prob = friend.user_prior
                            else:
                                friend_bayes_prob = friend_bayes_prob/sum_friend_bayes_prob
                            friend_predicted_rating = 0
                            for i in range(0, 10):
                                friend_predicted_rating += (i+1)/2 * friend_bayes_prob[i]
                            friend_predicted_rating = round(friend_predicted_rating*2)/2
                            ind_j = int(friend_predicted_rating*2.0 - 1.0)
                            multiply_rating_ji = np.multiply(multiply_rating_ji, np.transpose(
                                                                        user_object.user_friends_conditional_prob[:, ind_j]))

                user_bayes_prob = np.multiply(multiply_rating_ji, user_object.user_prior)
                sum_user_bayes_prob = np.sum(user_bayes_prob)
                if (sum_user_bayes_prob == 0):
                    user_bayes_prob = user_object.user_prior 
                else:
                    user_bayes_prob = user_bayes_prob/sum_user_bayes_prob

                predicted_rating = 0
                for i in range(0, 10):
                    predicted_rating += (i+1)/2 * user_bayes_prob[i]
                predicted_rating = round(predicted_rating*2)/2
                mean_squared_error += np.square((predicted_rating - user_business_dict[user_business].stars))
            testing_total_error += mean_squared_error 
            user_object.mean_squared_error = (mean_squared_error / valid_business_count)
            count_ratings += valid_business_count
            testing_total_error += user_object.mean_squared_error
            print("< user_id :", userId_Key, "mean_squared_error :", user_object.mean_squared_error, ">")
    testing_total_error = testing_total_error/count_ratings
    print(testing_total_error)

###############################################################################################################

def populate(user_file, review_file, target_user_dict):

    line_count = 0
    for user_line in user_file.readlines():
        if line_count%100000 == 0:
            print(line_count)
            sys.stdout.flush()
        user_object = json.loads(user_line)
        if len(user_object['friends']) >= 5:
            user_obj = User()
            user_obj.user_id = user_object['user_id']
            user_obj.user_friends_list = user_object['friends']
            friends_list = user_object['friends']
            if len(user_object['friends']) >= 20:
                # print(user_object['user_id'])
                # for now we consider at-most 20 friends
                friends_list = friends_list[:20]
            user_obj.user_friends_list = friends_list
            target_user_dict[user_object['user_id']] = user_obj
        line_count += 1

    # Mapping reviews to the user objects
    review_count = 0
    for review_line in review_file.readlines():
        
        if(review_count < 1888000):
            # training_business_data

            review_object = json.loads(review_line)
            if review_object['user_id'] in target_user_dict.keys():
                userObj = target_user_dict.get(review_object['user_id'])
                business_obj = Business()
                business_obj.business_id = review_object['business_id']
                business_obj.stars = review_object['stars']
                userObj.training_review_dict[review_object['business_id']] = business_obj
                target_user_dict[review_object['user_id']] = userObj

        else:
            # testing_business_data

            review_object = json.loads(review_line)
            if review_object['user_id'] in target_user_dict.keys():
                userObj = target_user_dict.get(review_object['user_id'])
                business_obj = Business()
                business_obj.business_id = review_object['business_id']
                business_obj.stars = review_object['stars']
                userObj.testing_review_dict[review_object['business_id']] = business_obj
                target_user_dict[review_object['user_id']] = userObj
        if review_count%1000000 ==0 :
            print(review_count)
            sys.stdout.flush()
        review_count += 1
    print("Crossed review_line")
    map_count = 0
    print(len(target_user_dict))
    for userId in target_user_dict.keys():
        user_obj = target_user_dict.get(userId)
        user_friends_list = []
        for friend_id in user_obj.user_friends_list:
            friend_obj = target_user_dict.get(friend_id)
            user_friends_list.append(friend_obj)
        user_obj.user_friends_list = user_friends_list
        target_user_dict[userId] = user_obj
        map_count+=1
        if map_count%10000 == 0:
            print(map_count)
    print("finished extracting from dataset")

main()
