Predicting Yelp star rating from social network using Bayesian Approach
CSE 740 Large Scale Machine Learning Seminar, Fall 2016

Pavan Behara,
pavankum@buffalo.edu 

Lalith Vikram Natarajan
lalithvi@buffalo.edu


Predicting Yelp star rating from user�s social network helps in building recommendation systems when the user wants to try out a new restaurant or for business analysis in predicting a new user behavior. The algorithm is based on Bayesian-inference based recommendation in social networks (Reference DOI: 10.1109/TPDS.2012.192). Report details the procedure and the code is attached.
