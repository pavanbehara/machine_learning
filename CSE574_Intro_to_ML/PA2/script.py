import numpy as np
from scipy.optimize import minimize
from scipy.io import loadmat
from numpy.linalg import det, inv
from math import sqrt, pi
import scipy.io
import matplotlib.pyplot as plt
import pickle
import sys

def ldaLearn(X,y):
    # Inputs
    # X - a N x d matrix with each row corresponding to a training example
    # y - a N x 1 column vector indicating the labels for each training example
    #
    # Outputs
    # means - A d x k matrix containing learnt means for each of the k classes
    # covmat - A single d x d learnt covariance matrix 
    
    # IMPLEMENT THIS METHOD
    labels = np.unique(y)
    N = X.shape[0]
    d = X.shape[1]
    k = labels.shape[0]
    means = []
    covmat = []
    covv = []
    for i in range(0,k):
        means.append(np.mean(X[y.flatten()==i+1],axis=0))
        covv = (np.cov(np.transpose(X[y.flatten()==i+1]),rowvar=1))
        if i==0:
           covmat = np.array(covv)
        else:
           covmat = np.array(covmat) + np.array(covv)
    covmat = np.divide(covmat,k)
    means = np.transpose(means)
    means.reshape(d,k) 
    covmat = np.array(covmat)  
    covmat.reshape(d,d)
    return means,covmat

def qdaLearn(X,y):
    # Inputs
    # X - a N x d matrix with each row corresponding to a training example
    # y - a N x 1 column vector indicating the labels for each training example
    #
    # Outputs
    # means - A d x k matrix containing learnt means for each of the k classes
    # covmats - A list of k d x d learnt covariance matrices for each of the k classes
    
    # IMPLEMENT THIS METHOD
    labels = np.unique(y)
    N = X.shape[0]
    d = X.shape[1]
    k = labels.shape[0]
    means = []
    covmats = []
    for i in range(0,k):
        means.append(np.mean(X[y.flatten()==i+1],axis=0))
        covmats.append(np.cov(np.transpose(X[y.flatten()==i+1]),rowvar=1))
    means = np.transpose(means)
    means.reshape(d,k)
    return means,covmats

def ldaTest(means,covmat,Xtest,ytest):
    # Inputs
    # means, covmat - parameters of the LDA model
    # Xtest - a N x d matrix with each row corresponding to a test example
    # ytest - a N x 1 column vector indicating the labels for each test example
    # Outputs
    # acc - A scalar accuracy value
    # ypred - N x 1 column vector indicating the predicted labels

    # IMPLEMENT THIS METHOD
    N = Xtest.shape[0]
    d = Xtest.shape[1]
    k = means.shape[1]
    ypred = []
    def Norm_f(x,mu,cov):
        Dd = cov.shape[0]
        x = np.reshape(x,[len(x),1])
        mu = np.reshape(mu,[len(mu),1])
        cov = np.reshape(cov.flatten(),[Dd,Dd])
        Norf = (1/(np.power(2*np.pi,Dd/2)*np.sqrt(np.linalg.det(cov))))*np.exp(-0.5*np.dot(np.transpose(np.array(x-mu)),np.dot(np.linalg.inv(cov),np.array((x-mu)))))
        return Norf
    acc = 0
    for i in range(0,N):
        tmp = []
        for j in range(0,k):
            tmp.append(Norm_f(Xtest[i,:],means[:,j],covmat))
        ypred.append(1+tmp.index(max(tmp)))
        acc = acc+int(ytest[i] == ypred[i])
    acc = (acc/N)
    ypred = np.array(ypred)
    return acc,ypred

def qdaTest(means,covmats,Xtest,ytest):
    # Inputs
    # means, covmats - parameters of the QDA model
    # Xtest - a N x d matrix with each row corresponding to a test example
    # ytest - a N x 1 column vector indicating the labels for each test example
    # Outputs
    # acc - A scalar accuracy value
    # ypred - N x 1 column vector indicating the predicted labels

    # IMPLEMENT THIS METHOD
    N = Xtest.shape[0]
    d = Xtest.shape[1]
    k = means.shape[1]
    ypred = []
    def Norm_f(x,mu,cov):
        Dd = cov.shape[0]
        x = np.reshape(x,[len(x),1])
        mu = np.reshape(mu,[len(mu),1])
        cov = np.reshape(cov.flatten(),[Dd,Dd])
        Norf = (1/(np.power(2*np.pi,Dd/2)*np.sqrt(np.linalg.det(cov))))*np.exp(-0.5*np.dot(np.transpose(np.array(x-mu)),np.dot(np.linalg.inv(cov),np.array((x-mu)))))
        return Norf
    acc = 0
    for i in range(0,N):
        tmp = []
        for j in range(0,k):
            tmp.append(Norm_f(Xtest[i,:],means[:,j],covmats[j]))
        ypred.append(1+tmp.index(max(tmp)))
        acc = acc+int(ytest[i] == ypred[i])
    acc = (acc/N)
    ypred = np.array(ypred) 
    return acc,ypred

def learnOLERegression(X,y):
    # Inputs:                                                         
    # X = N x d 
    # y = N x 1                                                               
    # Output: 
    # w = d x 1                                                                
    # IMPLEMENT THIS METHOD             
    XTX = np.dot(np.transpose(X),X)
    A = inv(XTX)
    w = np.dot(A,np.dot(np.transpose(X),y))
    return w

def learnRidgeRegression(X,y,lambd):
    # Inputs:
    # X = N x d                                                               
    # y = N x 1 
    # lambd = ridge parameter (scalar)
    # Output:                                                                  
    # w = d x 1                                                                

    # optimal analytic w
    XTX = np.dot(np.transpose(X), X)
    lI = lambd * np.eye(XTX.shape[0])
    A = inv(XTX +lI)
    w = np.dot( A , np.dot( np.transpose(X), y) )                                                       
    return w

def testOLERegression(w,Xtest,ytest):
    # Inputs:
    # w = d x 1
    # Xtest = N x d
    # ytest = X x 1
    # Output:
    # rmse
    N = Xtest.shape[0]
    rmse = 0
    for i in range( N ):
        X = Xtest[i][:,np.newaxis]
        wTx = np.dot(np.transpose(w) , X)
        rmse += ( ytest[i] - wTx)**2
    rmse = rmse / float(N)
    rmse = np.sqrt( rmse )          
    return rmse

def regressionObjVal(w, X, y, lambd):

    # compute squared error (scalar) and gradient of squared error with respect
    # to w (vector) for the given data X and y and the regularization parameter
    # lambda                                                                  
    Xw = np.dot(X,np.transpose(w))
    Xw = Xw[:,np.newaxis]
    A = y - Xw
    wTw = np.dot( np.transpose(w), w )
    error = 1./2 * np.dot( np.transpose(A), A ) + 1./2 * wTw
    error = error.item()
    # computing gradient
    XTXw = np.dot( np.dot( np.transpose(X) , X) , w )
    XTy = np.dot( np.transpose(X), y ).flatten()
    error_grad = XTXw - XTy + lambd * w                                          
    return error, error_grad

def mapNonLinear(x,d):
    # Inputs:                                                                  
    # x - a single column vector (N x 1)                                       
    # p - integer (>= 0)                                                       
    # Outputs:                                                                 
    # Xd - (N x (d+1))                                                         
    Xd = np.zeros((x.shape[0], d+1))
    for i in range(d+1):
        Xd[:,i] = x.flatten()**i
    return Xd

# Main script
#pdb.set_trace()
# Problem 1
# load the sample data                                                                 
if sys.version_info.major == 2:
    X,y,Xtest,ytest = pickle.load(open('sample.pickle','rb'))
else:
    X,y,Xtest,ytest = pickle.load(open('sample.pickle','rb'),encoding = 'latin1')

# LDA
means,covmat = ldaLearn(X,y)
ldaacc = ldaTest(means,covmat,Xtest,ytest)
print('LDA Accuracy = '+str(ldaacc))
# QDA
means,covmats = qdaLearn(X,y)
qdaacc = qdaTest(means,covmats,Xtest,ytest)
print('QDA Accuracy = '+str(qdaacc))

# plotting boundaries
x1 = np.linspace(-5,20,100)
x2 = np.linspace(-5,20,100)
xx1,xx2 = np.meshgrid(x1,x2)
xx = np.zeros((x1.shape[0]*x2.shape[0],2))
xx[:,0] = xx1.ravel()
xx[:,1] = xx2.ravel()

zacc,zldares = ldaTest(means,covmat,xx,np.zeros((xx.shape[0],1)))
plt.contourf(x1,x2,zldares.reshape((x1.shape[0],x2.shape[0])))
plt.scatter(Xtest[:,0],Xtest[:,1],c=ytest)

zacc,zqdares = qdaTest(means,covmats,xx,np.zeros((xx.shape[0],1)))
plt.contourf(x1,x2,zqdares.reshape((x1.shape[0],x2.shape[0])))
plt.scatter(Xtest[:,0],Xtest[:,1],c=ytest)

# Problem 2

if sys.version_info.major == 2:
    X,y,Xtest,ytest = pickle.load(open('diabetes.pickle','rb'))
else:
    X,y,Xtest,ytest = pickle.load(open('diabetes.pickle','rb'),encoding = 'latin1')

# add intercept
X_i = np.concatenate((np.ones((X.shape[0],1)), X), axis=1)
Xtest_i = np.concatenate((np.ones((Xtest.shape[0],1)), Xtest), axis=1)

w = learnOLERegression(X,y)
mle = testOLERegression(w,Xtest,ytest)

w_i = learnOLERegression(X_i,y)
mle_i = testOLERegression(w_i,Xtest_i,ytest)

print('RMSE without intercept '+str(mle))
print('RMSE with intercept '+str(mle_i))

# Problem 3
k = 101
lambdas = np.linspace(0, 1, num=k)
i = 0
rmses3 = np.zeros((k,1))
for lambd in lambdas:
    w_l = learnRidgeRegression(X_i,y,lambd)
    rmses3[i] = testOLERegression(w_l,Xtest_i,ytest)
    i = i + 1
plt.plot(lambdas,rmses3)

# Problem 4
k = 101
lambdas = np.linspace(0, 1, num=k)
i = 0
rmses4 = np.zeros((k,1))
opts = {'maxiter' : 100}    # Preferred value.                                                
w_init = np.ones((X_i.shape[1],1))
for lambd in lambdas:
    args = (X_i, y, lambd)
    w_l = minimize(regressionObjVal, w_init, jac=True, args=args,method='CG', options=opts)
    w_l = np.transpose(np.array(w_l.x))
    w_l = np.reshape(w_l,[len(w_l),1])
    rmses4[i] = testOLERegression(w_l,Xtest_i,ytest)
    i = i + 1
plt.plot(lambdas,rmses4)

# Problem 5
pmax = 7
lambda_opt = lambdas[np.argmin(rmses4)]
rmses5 = np.zeros((pmax,2))
for p in range(pmax):
    Xd = mapNonLinear(X[:,2],p)
    Xdtest = mapNonLinear(Xtest[:,2],p)
    w_d1 = learnRidgeRegression(Xd,y,0)
    rmses5[p,0] = testOLERegression(w_d1,Xdtest,ytest)
    w_d2 = learnRidgeRegression(Xd,y,lambda_opt)
    rmses5[p,1] = testOLERegression(w_d2,Xdtest,ytest)
plt.plot(range(pmax),rmses5)
plt.legend(('No Regularization','Regularization'))