import numpy as np
from scipy.optimize import minimize
from scipy.io import loadmat
from math import sqrt

def initializeWeights(n_in,n_out):
    """
    # initializeWeights return the random weights for Neural Network given the
    # number of node in the input layer and output layer

    # Input:
    # n_in: number of nodes of the input layer
    # n_out: number of nodes of the output layer

    # Output:
    # W: matrix of random initial weights with size (n_out x (n_in + 1))"""

    epsilon = sqrt(6) / sqrt(n_in + n_out + 1);
    W = (np.random.rand(n_out, n_in + 1)*2* epsilon) - epsilon;
    return W

def sigmoid(z):

    """# Notice that z can be a scalar, a vector or a matrix
    # return the sigmoid of input z"""

    return  np.divide(1,1+np.exp(-z))

def preprocess():
    """ Input:
     Although this function doesn't have any input, you are required to load
     the MNIST data set from file 'mnist_all.mat'.

     Output:
     train_data: matrix of training set. Each row of train_data contains
       feature vector of a image
     train_label: vector of label corresponding to each image in the training
       set
     validation_data: matrix of training set. Each row of validation_data
       contains feature vector of a image
     validation_label: vector of label corresponding to each image in the
       training set
     test_data: matrix of training set. Each row of test_data contains
       feature vector of a image
     test_label: vector of label corresponding to each image in the testing
       set

     Some suggestions for preprocessing step:
     - divide the original data set to training, validation and testing set
           with corresponding labels
     - convert original data set from integer to double by using double()
           function
     - normalize the data to [0, 1]
     - feature selection"""

    mat = loadmat('mnist_all.mat') #loads the MAT object as a Dictionary
    # Merges input training and test data into a single matrix and creates corresponding labels
    input_train_data = mat.get('train0')
    input_train_label = np.full((input_train_data.shape[0]), 0, dtype=np.int)
    test_data = mat.get('test0')
    test_label = np.full((test_data.shape[0]), 0, dtype=np.int)
    for i in range(1,10):
        m1 = mat.get('train'+str(i))
        input_train_data = np.concatenate((input_train_data, m1))
        input_train_label = np.append(input_train_label, np.full((m1.shape[0]), i, dtype=np.int))
        m2 = mat.get('test'+str(i))
        test_data = np.concatenate((test_data, m2))
        test_label = np.append(test_label, np.full((m2.shape[0]), i, dtype=np.int))

    #Pick a reasonable size for validation data
    validation_size = input_train_data.shape[0] / 6

    # Randomly split input training data into training data and validation data
    p = np.random.permutation(range(input_train_data.shape[0]))
    train_data = input_train_data[p[validation_size:],:]
    train_label = input_train_label[p[validation_size:]]
    validation_data = input_train_data[p[0:validation_size],:]
    validation_label = input_train_label[p[0:validation_size]]

    # Convert data from int to double and normalize to [0, 1]
    train_data = np.float64(train_data) / 255
    validation_data = np.float64(validation_data) / 255
    test_data = np.float64(test_data) / 255

    # Feature Selection: Remove all data points which are equal for all elements of train_data
    same_val_all = np.all(train_data == train_data[0,:], axis=0)
    train_data = train_data[:,~same_val_all]
    validation_data = validation_data[:,~same_val_all]
    test_data = test_data[:,~same_val_all]

    return train_data, train_label, validation_data, validation_label, test_data, test_label

def nnObjFunction(params, *args):
    """% nnObjFunction computes the value of objective function (negative log
    %   likelihood error function with regularization) given the parameters
    %   of Neural Networks, thetraining data, their corresponding training
    %   labels and lambda - regularization hyper-parameter.

    % Input:
    % params: vector of weights of 2 matrices w1 (weights of connections from
    %     input layer to hidden layer) and w2 (weights of connections from
    %     hidden layer to output layer) where all of the weights are contained
    %     in a single vector.
    % n_input: number of node in input layer (not include the bias node)
    % n_hidden: number of node in hidden layer (not include the bias node)
    % n_class: number of node in output layer (number of classes in
    %     classification problem
    % training_data: matrix of training data. Each row of this matrix
    %     represents the feature vector of a particular image
    % training_label: the vector of truth label of training images. Each entry
    %     in the vector represents the truth label of its corresponding image.
    % lambda: regularization hyper-parameter. This value is used for fixing the
    %     overfitting problem.

    % Output:
    % obj_val: a scalar value representing value of error function
    % obj_grad: a SINGLE vector of gradient value of error function
    % NOTE: how to compute obj_grad
    % Use backpropagation algorithm to compute the gradient of error function
    % for each weights in weight matrices.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % reshape 'params' vector into 2 matrices of weight w1 and w2
    % w1: matrix of weights of connections from input layer to hidden layers.
    %     w1(i, j) represents the weight of connection from unit j in input
    %     layer to unit i in hidden layer.
    % w2: matrix of weights of connections from hidden layer to output layers.
    %     w2(i, j) represents the weight of connection from unit j in hidden
    %     layer to unit i in output layer."""
    n_input, n_hidden, n_class, training_data, training_label, lambdaval = args
    w1 = params[0:n_hidden * (n_input + 1)].reshape((n_hidden, n_input + 1))
    w2 = params[(n_hidden * (n_input + 1)):].reshape((n_class, n_hidden + 1))
    obj_val = 0

    # Change training_label vector to format needed for this function
    tmp = np.zeros((training_data.shape[0], n_class))
    for n in range(0, training_data.shape[0]):
        tmp[n,training_label[n]] = 1
    training_label = tmp
    
    training_data = np.concatenate((training_data, np.ones((training_data.shape[0], 1))), axis=1)

    jpw2 = np.zeros((training_data.shape[0], n_class, n_hidden))
    jpw1 = np.zeros((training_data.shape[0], n_hidden, n_input))
    
    hiddennode_val = sigmoid(np.dot(training_data, w1.T))
    hiddennode_val = np.concatenate((hiddennode_val, np.ones((hiddennode_val.shape[0], 1))), axis=1)
    outnode_val = sigmoid(np.dot(hiddennode_val, w2.T))
    jval = 0.5 * np.sum(np.square(training_label-outnode_val))
    delta=(training_label-outnode_val)*outnode_val*(1-outnode_val)
    for k in range(training_data.shape[0]):
        jpw2[k] = -1 * np.outer(delta[k],hiddennode_val[k,:-1])
        tmp = -1* (1-hiddennode_val[k,:-1])*hiddennode_val[k,:-1]*np.sum(delta[k]*w2[:,:-1].T, axis=1)
        jpw1[k] = np.outer(tmp, training_data[k,:-1])

    n = training_data.shape[0]
    total_j = (1.0/n)*np.sum(jval)
    obj_val = total_j + lambdaval/(2.0*n)*(np.sum(np.square(w1))+np.sum(np.square(w2)))
    
    grad_w2 = (1.0/n) * (np.sum(jpw2, axis=0)+lambdaval*w2[:,:-1])
    grad_w2 = np.concatenate((grad_w2, np.zeros((w2.shape[0], 1))), axis=1)

    grad_w1 = (1.0/n) * (np.sum(jpw1, axis=0)+lambdaval*w1[:,:-1])
    grad_w1 = np.concatenate((grad_w1, np.zeros((w1.shape[0], 1))), axis=1)

    #Make sure you reshape the gradient matrices to a 1D array. for instance if your gradient matrices are grad_w1 and grad_w2
    #you would use code similar to the one below to create a flat array
    obj_grad = np.concatenate((grad_w1.flatten(), grad_w2.flatten()), 0)

    return (obj_val,obj_grad)
    
def nnPredict(w1,w2,data):

    """% nnPredict predicts the label of data given the parameter w1, w2 of Neural
    % Network.

    % Input:
    % w1: matrix of weights of connections from input layer to hidden layers.
    %     w1(i, j) represents the weight of connection from unit i in input
    %     layer to unit j in hidden layer.
    % w2: matrix of weights of connections from hidden layer to output layers.
    %     w2(i, j) represents the weight of connection from unit i in input
    %     layer to unit j in hidden layer.
    % data: matrix of data. Each row of this matrix represents the feature
    %       vector of a particular image

    % Output:
    % label: a column vector of predicted labels"""

    # Compute output nodes, then find maximum of output nodes to predict labels for input data,
    # adding bias to input data and hidden nodes
    hidden = sigmoid(np.dot(np.concatenate((data, np.ones((data.shape[0], 1))), axis=1), w1.T))
    output = sigmoid(np.dot(np.concatenate((hidden, np.ones((hidden.shape[0], 1))), axis=1), w2.T))
    labels = np.argmax(output, axis=1)

    return labels




"""**************Neural Network Script Starts here********************************"""

train_data, train_label, validation_data,validation_label, test_data, test_label = preprocess();


#  Train Neural Network

# set the number of nodes in input unit (not including bias unit)
n_input = train_data.shape[1];

# set the number of nodes in hidden unit (not including bias unit)
n_hidden = 12;

# set the number of nodes in output unit
n_class = 10;

# initialize the weights into some random matrices
initial_w1 = initializeWeights(n_input, n_hidden);
initial_w2 = initializeWeights(n_hidden, n_class);

# unroll 2 weight matrices into single column vector
initialWeights = np.concatenate((initial_w1.flatten(), initial_w2.flatten()),0)

# set the regularization hyper-parameter
lambdaval = 0.2;


args = (n_input, n_hidden, n_class, train_data, train_label, lambdaval)

#Train Neural Network using fmin_cg or minimize from scipy,optimize module. Check documentation for a working example

opts = {'maxiter' : 50}    # Preferred value.

nn_params = minimize(nnObjFunction, initialWeights, jac=True, args=args,method='CG', options=opts)

#In Case you want to use fmin_cg, you may have to split the nnObjectFunction to two functions nnObjFunctionVal
#and nnObjGradient. Check documentation for this function before you proceed.
#nn_params, cost = fmin_cg(nnObjFunctionVal, initialWeights, nnObjGradient,args = args, maxiter = 50)


#Reshape nnParams from 1D vector into w1 and w2 matrices
w1 = nn_params.x[0:n_hidden * (n_input + 1)].reshape( (n_hidden, (n_input + 1)))
w2 = nn_params.x[(n_hidden * (n_input + 1)):].reshape((n_class, (n_hidden + 1)))


#Test the computed parameters

predicted_label = nnPredict(w1,w2,train_data)

#find the accuracy on Training Dataset

print('\n Training set Accuracy:' + str(100*np.mean((predicted_label == train_label)).astype(float)) + '%')

predicted_label = nnPredict(w1,w2,validation_data)

#find the accuracy on Validation Dataset

print('\n Validation set Accuracy:' + str(100*np.mean((predicted_label == validation_label)).astype(float)) + '%')


predicted_label = nnPredict(w1,w2,test_data)

#find the accuracy on Validation Dataset

print('\n Test set Accuracy:' + str(100*np.mean((predicted_label == test_label)).astype(float)) + '%')
